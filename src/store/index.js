import { createStore } from "vuex"
import modulo from "@/store/modulo/index"

const store = createStore({
    modules: {
        modulo
    }
})

export default store